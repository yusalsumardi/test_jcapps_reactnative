import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Register, RegisterPassword } from '../pages'

const Router = createStackNavigator(
    {
        Register: {
            screen: Register,
        },
        RegisterPassword: {
            screen: RegisterPassword,
        }
    },
    {
        initialRouteName: 'Register'
    }
);

export default createAppContainer(Router);