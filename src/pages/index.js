import Register from './register';
import RegisterPassword from './registerPassword';

export {
    Register, RegisterPassword
}