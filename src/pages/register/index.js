import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, Alert } from 'react-native';
import RadioForm from 'react-native-simple-radio-button';
import { FloatingLabelInput } from 'react-native-floating-label-input';


class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: "",
            lastName: "",
            email: "",
            phoneNumber: "",
            birth_Day: "",
            birth_Month: "",
            birth_Year: "",
            gender: "Male",
            show: false,
        };
    };

    onBlur() {
        console.log('#####: onBlur');
    }


    render() {
        var radio_props = [
            { label: 'Male', value: 'Male' },
            { label: 'Female', value: 'Female' },
        ];
        return (
            <View style={styles.container}>
                <ScrollView >
                    <View>
                        <Text style={styles.h1title}>Personal Information</Text>
                        <Text style={styles.h3title}>Tell us a bit about yourself</Text>
                    </View>
                    <View>
                        <FloatingLabelInput
                            label="firstName"
                        // value={this.state.firstName}
                        // onChangeText={(value) => this.setState({ firstName: value })}
                        />
                    </View>
                    <View style={styles.SecRow}>
                        <TextInput
                            label="firstName"
                            onChangeText={(value) => this.setState({ firstName: value })}
                            value={this.state.firstName}
                            maxLength={50}
                            placeholder={"Your Name"} placeholderTextColor="#adb5db" style={styles.textinputName}
                        />
                        <TextInput
                            label="lastName"
                            onChangeText={(value) => this.setState({ lastName: value })}
                            value={this.state.lastName}
                            maxLength={50}
                            placeholder={"Last Name"} placeholderTextColor="#adb5db" style={styles.textinputName}
                        />
                    </View>
                    <View>
                        <TextInput placeholder={"Email Address"} placeholderTextColor="#adb5db" maxLength={50} style={styles.textinputGeneral} onChangeText={(value) => this.setState({ email: value })}
                            value={this.state.email} />
                    </View>
                    <View>
                        <TextInput placeholder={"Phone Number"} keyboardType={"numeric"} placeholderTextColor="#adb5db" maxLength={13} style={styles.textinputGeneral} onChangeText={(value) => this.setState({ phoneNumber: value })}
                            value={this.state.phoneNumber} />
                    </View>
                    <View>
                        <Text style={styles.h2title}>
                            Date of Birth (optional)
                        </Text>
                        <View style={styles.SecRow}>
                            <TextInput
                                label="birth_Day"
                                onChangeText={(value) => this.setState({ birth_Day: value })}
                                value={this.state.birth_Day}
                                maxLength={2}
                                keyboardType={"numeric"}
                                placeholder={"Day"} placeholderTextColor="#adb5db" style={styles.textinputBirth}
                            />
                            <TextInput
                                label="birth_Month"
                                onChangeText={(value) => this.setState({ birth_Month: value })}
                                value={this.state.birth_Month}
                                maxLength={2}
                                keyboardType={"numeric"}
                                placeholder={"Month"} placeholderTextColor="#adb5db" style={styles.textinputBirth}
                            />
                            <TextInput
                                label="birth_Year"
                                onChangeText={(value) => this.setState({ birth_Year: value })}
                                value={this.state.birth_Year}
                                maxLength={4}
                                keyboardType={"numeric"}
                                placeholder={"Year"} placeholderTextColor="#adb5db" style={styles.textinputBirth}
                            />
                        </View>
                    </View>
                    <View>
                        <Text style={styles.h2title}>
                            Gender (optional)
                        </Text>
                        <View>
                            <RadioForm
                                radio_props={radio_props}
                                initial={0}
                                formHorizontal={true}
                                buttonColor={'#adb5db'}
                                buttonInnerColor={'#fff'}
                                labelColor={'#adb5db'}
                                selectedButtonColor={'#fff'}
                                buttonInnerColor={'#e74c3c'}
                                RadioButtonLabel={'#fff'}
                                labelStyle={{ fontSize: 16, color: '#adb5db' }}
                                buttonSize={10}
                                buttonOuterSize={20}
                                onPress={(value) => { this.setState({ gender: value }) }}
                                style={styles.radioSex}
                            />
                        </View>
                    </View>
                    <View style={{ paddingBottom: 50, paddingTop: 10 }}>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => this.props.navigation.navigate('RegisterPassword', {
                                user: this.state
                            })}
                        >
                            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Continue</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#212529',
        // height: '100%',
    },
    h1title: {
        paddingTop: 20,
        color: '#fff',
        paddingLeft: 20,
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 5,
    },
    h3title: {
        color: '#f8f9fa',
        paddingLeft: 20,
        marginBottom: 20,
    },
    textinputName: {
        color: '#f8f9fa',
        borderColor: '#6c757d',
        borderWidth: 2,
        borderRadius: 10,
        paddingLeft: 20,
        paddingRight: 20,
        width: '48%',
        height: 70,
        fontSize: 16,

    },
    SecRow: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 20,
        marginBottom: 10,
    },
    textinputGeneral: {
        color: '#f8f9fa',
        borderColor: '#6c757d',
        borderWidth: 2,
        borderRadius: 10,
        paddingLeft: 20,
        paddingRight: 20,
        height: 70,
        fontSize: 16,
        marginHorizontal: 20,
        marginBottom: 10,
    },
    textinputBirth: {
        color: '#f8f9fa',
        borderColor: '#6c757d',
        borderWidth: 2,
        borderRadius: 10,
        paddingLeft: 20,
        paddingRight: 20,
        width: '30%',
        height: 70,
        fontSize: 16,
    },
    h2title: {
        color: '#f8f9fa',
        paddingLeft: 20,
        fontSize: 16,
        paddingVertical: 10,
    },
    radioSex: {
        color: '#f8f9fa',
        borderColor: '#6c757d',
        borderWidth: 2,
        borderRadius: 10,
        marginHorizontal: 20,
        height: 70,
        fontSize: 16,
        justifyContent: 'space-around',
        alignItems: 'center',
        marginBottom: 20,
    },
    button: {
        height: 40,
        color: '#000',
        width: '90%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f0ebd8',
        alignSelf: 'center',
    },
});

export default index;