import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet, TouchableOpacity, TextInput, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

class index extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        let reg = /^([@#](?=[^aeiou]{7,13}$)(?=[[:alnum:]]{7,13}$)(?=.*[A-Z]{1,}.*$).+)$/
        if (reg.test(event.target.value)) {
            this.setState({ value: event.target.value });
        }
    }


    componentDidMount() {
        const { navigation } = this.props;
        const user = navigation.getParam('user')
        this.setState({
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            phoneNumber: user.phoneNumber,
            birthDate: user.birth_Year + "-" + user.birth_Month + "-" + user.birth_Day,
            gender: user.gender,
            regexValid: true,
            validPassword: true,
            hidePass: true,
            data: null,
        });
    }

    _changeIcon = () => {
        this.setState({
            hidePass: !this.state.hidePass
        })
    }

    validationField = () => {
        if (this.state.regexValid) {
            return styles.textinputGeneral
        } else {
            return styles.textinputGeneralFalse
        }
    }



    validationPassword = () => {
        let reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/
        console.log(this.state.firstPassword);
        console.log(this.state.confirmPassword);
        if (reg.test(this.state.firstPassword)) {
            console.log("Sesuai Regex")
            this.setState({
                regexValid: true,
            })
            console.log(this.state)
            if (this.state.firstPassword != null && this.state.firstPassword != ("") && this.state.firstPassword === this.state.confirmPassword) {
                console.log("Password Valid")
                this.setState({
                    validPassword: true,
                })
                return true;
            } else {
                console.log("Password Confirm Salah");
                this.setState({
                    validPassword: false,
                })
                return false;
            }
        } else {
            this.setState({
                regexValid: false,
                validPassword: true,
            })
            console.log("Tidak sesuai Regex")
            return false;
        }

    }

    userRegister = () => {
        if (this.validationPassword()) {
            try {
                fetch('http://localhost:8080/api/register', {
                    method: 'POST',
                    headers: { 'Content-type': 'application/json; charset=UTF-8' },
                    body: JSON.stringify({
                        firstName: this.state.firstName,
                        lastName: this.state.lastName,
                        email: this.state.email,
                        phoneNumber: this.state.phoneNumber,
                        birthDate: this.state.birthDate,
                        gender: this.state.gender,
                        password: this.state.confirmPassword,
                    })
                })
                    .then(response => response.json())
                    .then(data => console.log("data dari json: " + data.password))
                Alert.alert('REGISTER SUCCESS')
            } catch (e) {
                console.log(e)
            }
        }
    }

    render() {
        const { validPassword, hidePass } = this.state;
        return (
            <View style={{ height: '100%' }}>
                <ScrollView style={styles.container}>
                    <View>
                        <Text style={styles.h1title}>
                            Create Password
                        </Text>
                        <Text style={styles.h2title}>
                            Password should be at least 8 characters long, and include a number and a special character.
                        </Text>
                    </View>
                    <View>
                        <View style={this.validationField()}>
                            <TextInput placeholder={"Password"} secureTextEntry={hidePass ? true : false} style={{ color: '#f8f9fa', fontSize: 16, width: '85%' }} placeholderTextColor="#adb5db" maxLength={50} onChangeText={(value) => this.setState({ firstPassword: value })}
                                value={this.state.firstPassword} />
                            <Icon
                                name={hidePass ? "eye" : "eye-off"}
                                size={20}
                                color="grey"
                                onPress={() => this._changeIcon()}
                            />
                        </View>
                        <View>
                            <TextInput placeholder={"Confirm Password"} secureTextEntry={hidePass ? true : false} placeholderTextColor="#adb5db" maxLength={50} style={validPassword ? styles.textinputGeneral : styles.textinputGeneralFalse} onChangeText={(value) => this.setState({ confirmPassword: value })}
                                value={this.state.confirmPassword} />
                        </View>
                    </View>


                    <View>
                        <Text style={{ color: validPassword ? '#212529' : '#b77272', paddingLeft: 40, marginTop: 10 }}>
                            Password does not match
                        </Text>
                    </View>
                </ScrollView>
                <View>
                    <View style={styles.buttonPosition}>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => this.userRegister()}
                        >
                            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Create Account</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#212529',
    },
    button: {
        height: 40,
        color: '#000',
        width: '90%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f0ebd8',
        alignSelf: 'center',
    },
    buttonPosition: {
        position: 'absolute',
        bottom: 0,
        height: 40,
        justifyContent: 'center',
        width: '100%',
        alignSelf: 'center',
        marginBottom: 20,
    },
    h1title: {
        paddingTop: 20,
        color: '#fff',
        paddingLeft: 20,
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 5,
    },
    h2title: {
        color: '#f8f9fa',
        paddingLeft: 20,
        fontSize: 16,
        marginBottom: 30,
    },
    textinputGeneral: {
        color: '#f8f9fa',
        borderColor: '#6c757d',
        borderWidth: 2,
        borderRadius: 10,
        paddingLeft: 20,
        paddingRight: 20,
        height: 70,
        fontSize: 16,
        marginHorizontal: 20,
        marginBottom: 10,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    textinputGeneralFalse: {
        color: '#f8f9fa',
        borderColor: '#b77272',
        borderWidth: 2,
        borderRadius: 10,
        paddingLeft: 20,
        paddingRight: 20,
        height: 70,
        fontSize: 16,
        marginHorizontal: 20,
        marginBottom: 10,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
})

export default index;